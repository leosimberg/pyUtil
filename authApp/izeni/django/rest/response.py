__author__ = 'Leonardo Simberg'
__date__ = '07/09/2013'
__version__ = 1.0

"""
*
*  Standard anwsers for rest services using Django Rest
*
*  This code is based on Twitter API error codes and responses
*     https://dev.twitter.com/docs/error-codes-responses
*
"""

from rest_framework.response import Response


# Success
# Status 200
def ok(data=None):
    return Response(data, status=200)


# There was no new data to return.
# Status 304
def notModified(message='', code=0):
    msg = dict()
    if message != '':
        msg['message'] = message
    if code != 0:
        msg['code'] = code
    return Response(msg, status=304)


# The request was invalid. An accompanying error message
# will explain why
# Status 400
def badRequest(message='', code=0):
    msg = dict()
    msg['message'] = message
    if code != 0:
        msg['code'] = code
    return Response(msg, status=400)


# Authentication credentials were missing or incorrect
# Status 401
def unauthorized(message='', code=0):
    msg = dict()
    msg['message'] = message
    if code != 0:
        msg['code'] = code
    return Response(msg, status=401)


# The request is understood, but it has been refused or access is
# not allowed. An accompanying error message will explain why.
# Status 403
def forbidden(message='', code=0):
    msg = dict()
    msg['message'] = message
    if code != 0:
        msg['code'] = code
    return Response(msg, status=403)


# The URI requested is invalid or the resource requested, such as
# a user, does not exists. Also returned when the requested format
# is not supported by the requested method.
# Status 404
def notFound(message='', code=0):
    msg = dict()
    msg['message'] = message
    if code != 0:
        msg['code'] = code
    return Response(msg, status=404)


# Something is broken.
# Status 500
def internalServerError(message='', code=0):
    msg = dict()
    msg['message'] = message
    if code != 0:
        msg['code'] = code
    return Response(msg, status=500)


# Something is broken.
# Status 503
def serviceUnavailable(message='', code=0):
    msg = dict()
    msg['message'] = message
    if code != 0:
        msg['code'] = code
    return Response(msg, status=503)
