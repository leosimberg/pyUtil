from django.db import models


class User(models.Model):
    first = models.CharField( max_length=50, )
    last = models.CharField( max_length=50, )
    email = models.CharField( max_length=50, )
    password = models.CharField( max_length=20, )
    token = models.CharField( max_length=200, blank=True, null=True, )
    active = models.BooleanField( default=True, )
    
    def __unicode__(self):
        return self.email
    
    
class LoginHistory(models.Model):
    user = models.ForeignKey( User )
    access = models.DateTimeField( blank=True, )
    
    
    
