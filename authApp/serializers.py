from rest_framework import serializers
from authApp.models import User, LoginHistory


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User

class LoginHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = LoginHistory


