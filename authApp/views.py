from ubuntuone.storageprotocol.errors import DoesNotExistError
from django.http import HttpResponse
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from authApp.models import User, LoginHistory
from authApp.serializers import UserSerializer, LoginHistorySerializer
from izeni.django.rest import response
import logging


def echo(request, word):
    return HttpResponse(word)


class UserList(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class LoginHistoryList(generics.ListCreateAPIView):
    queryset = LoginHistory.objects.all()
    serializer_class = LoginHistorySerializer


class LoginHistoryDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = LoginHistory.objects.all()
    serializer_class = LoginHistorySerializer


class Login(APIView):
    def post(self, request):
        email = request.DATA.get('email')
        password = request.DATA.get('password')
        if email is None or password is None:
            return response.badRequest("email or password is null")

        userSet = User.objects.filter(email=email)[:1]
        if len(userSet) == 0:
            return response.notFound("User not found with email = " + email, 21)

        user = userSet[0]
        if user.password == password:
            return response.ok({"user_id": user.id})
        return response.notFound("Password is incorrect!")


class AuthView(APIView):
    #authentication_classes = (SessionAuthentication, BasicAuthentication)
    authentication_classes = (TokenAuthentication, )
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        content = {
            'user': unicode(request.user),  # `django.contrib.auth.User` instance.
            'auth': unicode(request.auth),  # None
        }
        return Response(content)







"""

        try:
            data = parseRequest(request)
        except Exception as ret:
            return ret.args[0]
# So far, it only works with application/json, but I'd like to implement
# other parsers
def parseRequest(request):
    data = request.body
    logging.info("request.body: " + data)
    if data is None or data == '':
        raise Exception(response.badRequest("empty payload"))
    return _parseJson(data)


def _parseJson(data):
    try:
        data = json.loads(data)
    except:
        raise Exception(response.badRequest("payload format error"))
    return data

"""

