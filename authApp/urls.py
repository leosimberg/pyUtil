from django.conf.urls import patterns, include, url
from authApp import views

urlpatterns = patterns('',
    url(r'^echo/(?P<word>\w+)/$',views.echo, name='echo'),
    url(r'^data/user/$', views.UserList.as_view()),
    url(r'^data/user/(?P<pk>[0-9]+)/$', views.UserDetail.as_view()),
    url(r'^data/user/(?P<pk>[0-9]+)/$', views.UserDetail.as_view()),
    url(r'^data/loginhistory/(?P<pk>[0-9]+)/$', views.LoginHistoryDetail.as_view()),
    url(r'^data/login/$', views.Login.as_view()),
    url(r'^data/auth/$', views.AuthView.as_view()),
)


