from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^authApp/', include('authApp.urls', namespace="authApp")),
    url(r'^api-token-auth/', 'rest_framework.authtoken.views.obtain_auth_token')
)

